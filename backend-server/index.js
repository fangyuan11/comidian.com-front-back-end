var express = require('express');
var app = express();

app.use((req, res, next) => {
	res.set('Access-Control-Request-Method', 'GET,POST,PUT,DELETE');
	res.set('Access-Control-Allow-Headers', 'Authorization,Content-Type');
	res.set('Access-Control-Allow-Origin', 'http://localhost:63342');
	next()
});

app.param('user', (req, res, next, val) => {
	var userId = parseInt(val);
	if(!isNaN(userId)){
		req.userId = userId;
		next();
	}
	else if(val.length){
		req.username = val;
		next();
	}
	else{
		res.status(404).end();
	}
})

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/api/users', (req, res) => {
	res.json(users);
});

app.get('/api/users/:user', (req, res) => {
	var predicate = () => false;
	
	if(req.userId){
		console.log('lookup user - ', req.userId);
		predicate = u => req.userId == u.id;
	}
	else if(req.username){
		console.log('lookup user -', req.username);
		predicate = u => req.username == u.username;
	}
	
	var user = users.filter(predicate)[0];
	if(user) res.json(user);
	else res.status(404).end();
})

app.post('/api/authenticate', (req, res) => {
	res.json({success: true});
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

var users = [
	{
		username: 'name1',
		firstName: 'first name 1',
		lastName: 'last name 1',
		id: 1
	},
	{
		username: 'fang',
		firstName: 'fang',
		lastName: 'yuan',
		id: 2
	},
	{
		username: 'comsite',
		firstName: 'comsite first name',
		lastName: 'comsite last name',
		id: 2
	}
]